﻿using System;
using System.Collections.Generic;
using System.Text;

namespace com.ruovea.ddns.util
{
    public interface IDomainRecord
    {
        /// <summary>
        /// 获取 记录
        /// </summary>
        /// <param name="domainName"></param>
        /// <returns></returns>
        IEnumerable<DescribeRecord> GetRecords(string domainName);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="record"></param>
        void UpdateRecord(DescribeRecord record);
        /// <summary>
        /// 执行刷新程序
        /// </summary>
        void Refresh();
    }
}
